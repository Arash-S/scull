#ifndef __SCULL_DEVICE_H__
#define __SCULL_DEVICE_H__

#ifndef SCULL_DEVICE_NAME
#define SCULL_DEVICE_NAME "scull"
#endif

#ifndef SCULL_DEVICE_MAJOR
#define SCULL_DEVICE_MAJOR 0
#endif

#ifndef SCULL_NUMBER_DEVICES
#define SCULL_NUMBER_DEVICES 4
#endif

#ifndef SCULL_DATA_SET
#define SCULL_DATA_SET 1000
#endif

#ifndef SCULL_DATA_UNIT
#define SCULL_DATA_UNIT 4000
#endif

#define TYPE(minor) (((minor) >> 4) & 0xf)	/** High nibble */
#define NUM(minor) ((minor) & 0xf)	 	/** Low nibble */

/**
 * Configurable parameters 
 * for Simple Character Utility for Loading Localities (SCULL) devices.
 */
extern int scull_major;
extern int scull_number_devices;
extern int scull_data_set;
extern int scull_data_unit;

/**
 * Each device has one or more data segments.
 * There is a set of data units in each segment.
 */
struct scull_data_segment {
	void **set;
	struct scull_data_segment *next;
};

struct scull_device {
	unsigned int uid;

	struct mutex lock;
	struct cdev cdev;

	struct {
		int set; 	/** The size of the hole set */
		int unit; 	/** The size of the each data unit in the set */
		int data;  	/** Amount of data stored */
	} size;

	struct scull_data_segment *data;
};

ssize_t scull_read(struct file *, char __user *, size_t, loff_t *);
ssize_t scull_write(struct file *, const char __user *, size_t, loff_t *);
loff_t scull_llseek(struct file *, loff_t, int);

void scull_erase_data_segment(struct scull_device *);

#endif
