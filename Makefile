LDDINC=$(PWD)/include

EXTRA_CFLAGS += -I$(LDDINC)

ifneq ($(KERNELRELEASE),)

scull-objs := main.o
obj-m := scull.o

else

KDIR ?= /lib/modules/$(shell uname -r)/build
PWD := $(shell pwd)

modules:
	$(MAKE) -C $(KDIR) M=$(PWD) modules

endif

clean:
	rm -rf *.o *~ core .depend .*.cmd *.ko *.mod.c .tmp_versions *.mod modules.order *.symvers
