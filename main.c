#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/seq_file.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>

#include "device.h"
#include "base.h"

MODULE_AUTHOR("Alessandro Rubini, Jonathan Corebet, Arash Sadeghizadeh");
MODULE_LICENSE("Dual BSD/GPL");

int scull_major = SCULL_DEVICE_MAJOR;
int scull_minor = 0;
int scull_number_devices = SCULL_NUMBER_DEVICES;
int scull_data_set = SCULL_DATA_SET;
int scull_data_unit = SCULL_DATA_UNIT;

struct scull_device *scull_dev;

void scull_erase_data_segments(struct scull_device *dev)
{
	int i;
	struct scull_data_segment *data = dev->data;
	struct scull_data_segment *tmp = NULL;

	while (data) {
		if (data->set) {
			for (i = 0; i < dev->size.set; i++) {
				kfree(data->set[i]);
			}
			kfree(data->set);
			data->set = NULL;
		}

		tmp = data;
		data = data->next;
		kfree(tmp);
	}
}

void scull_take_data_segments(struct scull_device *dev, int n,
			      struct scull_data_segment **res)
{
	size_t seg_size = sizeof(struct scull_data_segment);
	struct scull_data_segment **seg = &dev->data;

	n = n+1;
	*res = NULL;
	while (n--) {
		if (*seg) {
			continue;
		}

		*seg = kmalloc(seg_size, GFP_KERNEL);
		if (!(*seg)) {
			return;
		}

		memset(*seg, 0, seg_size);
		if (n) {
			seg = &(*seg)->next;
		}
	}

	*res = *seg;
}

int scull_open(struct inode *inode, struct file *file)
{
	int res = SUCCESS;
	struct scull_device *dev;

	dev = container_of(inode->i_cdev, struct scull_device, cdev);
	file->private_data = dev;

	if (O_WRONLY == (O_ACCMODE & file->f_flags)) {
		if (mutex_lock_interruptible(&dev->lock)) {
			res = -ERESTARTSYS;
			goto EXIT;
		}

		scull_erase_data_segments(dev);
		mutex_unlock(&dev->lock);
	}

 EXIT:
	return res;
}

int scull_release(struct inode *inode, struct file *file)
{
	return SUCCESS;
}

ssize_t scull_read(struct file *file, char __user *buf,
		   size_t count, loff_t *pos)
{
	struct scull_data_segment *seg = NULL;
	struct scull_device *dev = file->private_data;

	int idx;
	int off;
	int tot = dev->size.set * dev->size.unit;
	ssize_t res = 0;

	if (mutex_lock_interruptible(&dev->lock)) {
		res = -ERESTARTSYS;
		goto EXIT;
	} else if (*pos >= dev->size.data) {
		goto CLEANUP;
	} else if (*pos + count > dev->size.data) {
		count = dev->size.data - *pos;
	}

	off = (long) *pos % tot;
	idx = off / dev->size.unit;
	off = off % dev->size.unit;

	scull_take_data_segments(dev, (long) *pos / tot, &seg);
	if (!seg || !seg->set || !seg->set[idx]) {
		goto CLEANUP;
	}

	if (dev->size.unit - off < count) {
		count = dev->size.unit - off;
	}

	if (copy_to_user(buf, seg->set[idx] + off, count)) {
		res = -EFAULT;
		goto CLEANUP;
	}

	*pos += count;
	res = count;

 CLEANUP:
	mutex_unlock(&dev->lock);
 EXIT:
	return res;
}

ssize_t scull_write(struct file *file, const char __user *buf,
		    size_t count, loff_t *pos)
{
	struct scull_data_segment *seg = NULL;
	struct scull_device *dev = file->private_data;

	int idx;
	int off;
	int tot = dev->size.set * dev->size.unit;
	int mem_size = dev->size.set * sizeof(char *);
	ssize_t res = -ENOMEM;

	if (mutex_lock_interruptible(&dev->lock)) {
		res = -ERESTARTSYS;
		goto EXIT;
	}
	
	off = (long) *pos % tot;
	idx = off / dev->size.unit;
	off = off % dev->size.unit;

	scull_take_data_segments(dev, (long) *pos / tot, &seg);
	if (!seg) {
		goto CLEANUP;
	}

	if (!seg->set) {
		seg->set = kmalloc(mem_size, GFP_KERNEL);
		if (!seg->set) {
			goto CLEANUP;
		}

		memset(seg->set, 0, mem_size);
	}

	if (!seg->set[idx]) {
		seg->set[idx] = kmalloc(dev->size.unit, GFP_KERNEL);
		if (!seg->set[idx]) {
			goto CLEANUP;
		}
	}

	if (dev->size.unit - off < count) {
		count = dev->size.unit - off;
	}

	if (copy_from_user(seg->set[idx] + off, buf, count)) {
		res = -EFAULT;
		goto CLEANUP;
	}

	*pos += count;
	res = count;

	if (dev->size.data < *pos) {
		dev->size.data = *pos;
	}
	
 CLEANUP:
	mutex_unlock(&dev->lock);
 EXIT:
	return res;
}

loff_t scull_llseek(struct file *file, loff_t off, int whence)
{
	loff_t new_pos;
	struct scull_device *dev = file->private_data;

	switch(whence) {
	case 0:		/** SEEK_SET */
		new_pos = off;
		break;
	case 1:		/** SEEK_CUR */
		new_pos = file->f_pos + off;
		break;
        case 2:		/** SEEK_END */
		new_pos = dev->size.data + off;
		break;
	default:
		goto FAILURE;
	}

	if (0 > new_pos) {
		goto FAILURE;
	}

	file->f_pos = new_pos;
	return new_pos;
 FAILURE:
	return -EINVAL;
}

struct file_operations scull_fops = {
	.owner = THIS_MODULE,
	.open = scull_open,
	.release = scull_release,
	.read = scull_read,
	.write = scull_write,
	.llseek = scull_llseek,
};

static void scull_setup_cdev(struct scull_device *dev, int i)
{
	int err = 0;
	int dev_no = MKDEV(scull_major, scull_minor + i);

	cdev_init(&dev->cdev, &scull_fops);

	dev->cdev.owner = THIS_MODULE;
	err = cdev_add(&dev->cdev, dev_no, 1);
	if (err) {
		printk(KERN_NOTICE "Error %d adding scull%d", err, i);
	}
}

void scull_cleanup_module(void)
{
	int i;
	dev_t dev_no = MKDEV(scull_major, scull_minor);

	if (scull_dev) {
		for (i = 0; i < scull_number_devices; i++) {
			scull_erase_data_segments(scull_dev + i);
			cdev_del(&scull_dev[i].cdev);
		}

		kfree(scull_dev);
	}

	unregister_chrdev_region(dev_no, scull_number_devices);
}

int scull_init_module(void)
{
	int i;
	int res;
	size_t mem_size = sizeof(struct scull_device);
	dev_t dev_no = 0;

	if (scull_major) {
		dev_no = MKDEV(scull_major, scull_minor);
		res = register_chrdev_region(dev_no, scull_number_devices,
					     SCULL_DEVICE_NAME);
	} else {
		res = alloc_chrdev_region(&dev_no, scull_minor,
					  scull_number_devices,
					  SCULL_DEVICE_NAME);
		scull_major = MAJOR(dev_no);
	}
	if (0 < res) {
		printk(KERN_WARNING "%s: can't get major %d\n",
		       SCULL_DEVICE_NAME, scull_major);
		goto EXIT;
	}

	scull_dev = kmalloc(scull_number_devices * mem_size, GFP_KERNEL);
	if (!scull_dev) {
		res = -ENOMEM;
		goto CLEANUP;
	}

	memset(scull_dev, 0, scull_number_devices * mem_size);

	for (i = 0; i < scull_number_devices; i++) {
		scull_dev[i].size.unit = scull_data_unit;
		scull_dev[i].size.set = scull_data_set;
		mutex_init(&scull_dev[i].lock);
		scull_setup_cdev(&scull_dev[i], i);
	}

	return SUCCESS;
 CLEANUP:
	scull_cleanup_module();
 EXIT:
	return res;
}

module_param(scull_major, int, S_IRUGO);
module_param(scull_minor, int, S_IRUGO);
module_param(scull_number_devices, int, S_IRUGO);
module_param(scull_data_set, int, S_IRUGO);
module_param(scull_data_unit, int, S_IRUGO);

module_init(scull_init_module);
module_exit(scull_cleanup_module);
